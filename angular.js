var custom_uploader;
var wcGriffithSubs = angular.module( 'wcGriffithSubs', [ 'ngSanitize' ] ).run(function ($rootScope){
	$rootScope.window = window;
});

wcGriffithSubs.filter('includedSheets', function() {
	return function(input) {
		includes = [];
		angular.forEach( input, function( value, key ){
			if( value.include )
				includes.push( value );
		});
		return includes;
	};
});

wcGriffithSubs.filter('unmatchedPackages', function() {
	return function( input ) {
		packages = {};
		angular.forEach( input, function( value, key ){
			if( !angular.isNumber( value ) )
				packages[key] = value;
		});
		return packages;
	}
});

wcGriffithSubs.filter('objectSize', function() {
	return function(input) {
		var size = 0, key;
		for (key in input) {
			if (input.hasOwnProperty(key)) size++;
		}
		return size;
	};
});

wcGriffithSubs.controller( 'ImportCtrl', [ '$scope', '$http', '$filter', '$sanitize', function( $scope, $http, $filter )
{
	$scope.columns = "";
	$scope.dataFile = "";
	$scope.data = null;
	$scope.dataType = '';
	$scope.loading = false;
	$scope.sheets = [];
	$scope.sheetData = null;
	$scope.packageData = null;
	$scope.userData = null;
	$scope.importNumber = 0;
	$scope.isNumber = angular.isNumber;
	$scope.firstUserGroup = 'recipient';
	$scope.sheetFormat = 'oldsite';

	$scope.loadData = function()
	{
		$scope.loading = true;

		var includedSheets = $filter( 'includedSheets' )( $scope.sheets );
		data = {
			action: 'get_sheet_data',
			sheets: includedSheets,
			firstUserGroup: $scope.firstUserGroup,
			datafile: $scope.dataFile.id
		};

		$http({ method: 'POST',  url: ajaxurl, params: {action: 'get_sheet_data'},  data: data }).
			success(function( data, status, headers, config )
			{
				$scope.loading = false;
				$scope.dataType = 'sheetData';
				$scope.columns = data.columns;
				delete data.columns;
				for( sheet in data ) {
					data[sheet] = jQuery.map(data[sheet], function(value, index) {
						return [value];
					});
				}
				$scope.sheetData = data;
			});
	};

	$scope.checkPackages = function()
	{
		$scope.loading = true;

		var includedSheets = $filter( 'includedSheets' )( $scope.sheets );
		data = {
			action: 'check_packages',
			sheets: includedSheets,
			firstUserGroup: $scope.firstUserGroup,
			datafile: $scope.dataFile.id,
			importNumber: $scope.importNumber,
			sheetFormat: $scope.sheetFormat
		};

		$http({ method: 'POST',  url: ajaxurl, params: {action: 'check_packages'},  data: data }).
			success(function( data, status, headers, config )
			{
				$scope.loading = false;
				$scope.dataType = 'packages'
				$scope.packageData = data;
			});

	};

	$scope.createOrders = function()
	{
		$scope.loading = true;

		var includedSheets = $filter( 'includedSheets' )( $scope.sheets );
		data = {
			action: 'create_orders',
			sheets: includedSheets,
			firstUserGroup: $scope.firstUserGroup,
			datafile: $scope.dataFile.id,
			importNumber: $scope.importNumber,
			sheetFormat: $scope.sheetFormat
		};

		$http({ method: 'POST',  url: ajaxurl, params: {action: 'create_orders'},  data: data }).
			success(function( data, status, headers, config )
			{
				$scope.loading = false;
				$scope.dataType = 'createdOrders';
				$scope.data = data;
				//$scope.apply();
				//$scope.data = angular.fromJson( data );
			});

	};

	$scope.checkUsers = function()
	{
		$scope.loading = true;

		var includedSheets = $filter( 'includedSheets' )( $scope.sheets );
		data = {
			action: 'check_users',
			sheets: includedSheets,
			firstUserGroup: $scope.firstUserGroup,
			datafile: $scope.dataFile.id,
			importNumber: $scope.importNumber,
			sheetFormat: $scope.sheetFormat
		};

		$http({ method: 'POST',  url: ajaxurl, params: {action: 'check_users'},  data: data }).
			success(function( data, status, headers, config )
			{
				$scope.loading = false;
				$scope.dataType = 'userData';
				$scope.userData = data;
				//$scope.apply();
				//$scope.data = angular.fromJson( data );
			});
	}

	$scope.uploadWindow = function( $event )
	{
		//If the uploader object has already been created, reopen the dialog
		if (custom_uploader) {
			custom_uploader.open();
			return;
		}

		//Extend the wp.media object
		custom_uploader = wp.media.frames.file_frame = wp.media({
			title: 'Choose File',
			button: { text: 'Choose File' },
			multiple: false
		});

		//When a file is selected, grab the URL and set it as the text field's value
		custom_uploader.on('select', function()
		{
			attachment = custom_uploader.state().get('selection').first().toJSON();
			$scope.dataFile = attachment;

			$scope.packageData = null;
			$scope.sheetData = null;

			data = {
				action: 'get_sheet_info',
				datafile: attachment.id
			};
			$http({ method: 'POST',  url: ajaxurl, params: {action: 'get_sheet_info'},  data: data }).
				success(function( data, status, headers, config )
				{
					$scope.sheets = angular.fromJson( data );
					angular.forEach( $scope.sheets, function( value, key ){
						$scope.sheets[key].include = false;
					});
				});
		});

		//Open the uploader dialog
		custom_uploader.open();
	};

}]);

wcGriffithSubs.controller( 'ReportCtrl', [ '$scope', '$http', '$filter', '$sanitize', function( $scope, $http, $filter )
{

	$scope.data = null;
	$scope.fields = [];
	$scope.loading = false;
	$scope.exporting = false;
	$scope.requestTime = 0;
	$scope.numResults = 0;
	$scope.reportName = "daily_dispatch";
	$scope.filters = {
		'numResults': false,
		'dateStart': false,
		'dateEnd' : false
	};

	var today = new Date();
	var yesterday = new Date( (new Date()).valueOf() - (1000*3600*24) );
	//$scope.startDate = ('0'+yesterday.getDate()).slice(-2) + '-' + ('0'+(yesterday.getMonth()+1)).slice(-2) + '-' + yesterday.getFullYear();
	$scope.startDate = ('0'+today.getDate()).slice(-2) + '-' + ('0'+(today.getMonth()+1)).slice(-2) + '-' + today.getFullYear();
	$scope.endDate = ('0'+today.getDate()).slice(-2) + '-' + ('0'+(today.getMonth()+1)).slice(-2) + '-' + today.getFullYear();

	jQuery('#start-date').datepicker({
		dateFormat: 'dd-mm-yy',
		onClose: function( dateText, instance ) {
			$scope.startDate = dateText;
		}
	});

	jQuery('#end-date').datepicker({
		dateFormat: 'dd-mm-yy',
		onClose: function( dateText, instance ) {
			$scope.endDate = dateText;
		}
	});

	$scope.logSubscriptionsData = function( $event )
	{
		$scope.loading = true;
		$scope.data = null;

		var startTime = new Date().getTime();

		var data = {
			action: 'gr_reports_json',
			limit: $scope.filters.numResults ? $scope.numResults : 0,
			type: $scope.getSubscriptionResultType(),
			status: $scope.getSubscriptionResultStatus(),
			fields: $scope.getSubscriptionResultFields( $scope.getSubscriptionResultType() )
		};

		if( $scope.filters.dateStart === true )
			data['startdate'] = $scope.startDate;

		if( $scope.filters.dateEnd )
			data.enddate = $scope.endDate;

		$http({ method: 'POST',  url: ajaxurl, params: {action: 'gr_reports_json'},  data: data }).
			success(function( data, status, headers, config )
			{
				$scope.requestTime = new Date().getTime() - startTime;
				$scope.loading = false;
				$scope.data = data;
				//console.log( $scope.fields );
				//console.log( jQuery.inArray('gifter', $scope.fields) );
				//$scope.data = angular.fromJson( data );
			});
	}

	$scope.openResultsWindow = function( $event )
	{
		var style = document.getElementById( "results-table-style" ).outerHTML;
		var divText = document.getElementById( "results-table" ).outerHTML;
		var myWindow = window.open('','','width='+window.screen.availWidth+',height='+window.screen.availHeight);
		var doc = myWindow.document;
		doc.open();
		doc.write(style);
		doc.write(divText);
		doc.close();
	}

	$scope.getCSV = function( $event )
	{
		/*
		$scope.loading = true;
		data = {
			action: 'gr_reports_csv',
			limit: $scope.numResults,
			type: $scope.getSubscriptionResultType(),
			status: $scope.getSubscriptionResultStatus()
		};

		$http({ method: 'POST',  url: ajaxurl, params: {action: 'gr_reports_csv'},  data: data }).
			success(function( data, status, headers, config )
			{
				$scope.loading = false;
				jQuery("body").append("<iframe src='" + retData.url; + "' style='display: none;' ></iframe>")
				return data;
				//console.log( $scope.data );
				//$scope.apply();
				//$scope.data = angular.fromJson( data );
			});*/
	}

	$scope.export = function( $event )
	{
		$scope.exporting = true;
		setTimeout( function(){
			$scope.exporting = false;
		}, $scope.requestTime );
	}

	$scope.getSubscriptionResultType = function()
	{
		var status = '';
		switch( $scope.reportName )
		{
			case 'lapsing':
				status = 'lasping_subscriptions';
				break;
			case 'expiring':
				status = 'expiring_subscriptions';
				break;
			case 'daily_dispatch':
				status = 'daily_dispatch';
				break;
			case 'bulk_dispatch':
				status = 'bulk_dispatch';
				break;
            case 'new':
                status = 'new_subscriptions';
                break;
			case 'all_inactive_subscriptions':
			case 'all_active_subscriptions':
			default:
				status = 'all_subscriptions';
				break;
		}
		return status;
	}

	$scope.getSubscriptionResultStatus = function()
	{
		var status = '';
		switch( $scope.reportName )
		{
			case 'all_inactive_subscriptions':
				status = 'inactive';
				break;
			default:
				status = 'active';
				break;
		}
		return status;
	}

	$scope.getSubscriptionResultFields = function( type )
	{
		var fields = '';
		switch( type )
		{
			case 'daily_dispatch':
			case 'bulk_dispatch':
			case 'lasping_subscriptions':
				fields = ['recipient', 'order'];
				break;
			case 'all_subscriptions':
			default:
				fields = ['recipient', 'gifter', 'order'];
				break;
		}
		$scope.fields = fields;
		return fields;
	}

	$scope.getNumResults = function()
	{
		var size = 0;

		if( 'subscriptions' in $scope.data )
			size += Object.size( $scope.data.subscriptions );

		if( 'editions' in $scope.data )
			size += Object.size( $scope.data.editions );

		if( 'digital_singles' in $scope.data )
			size += Object.size( $scope.data.digital_singles );

		return size;
	}

	$scope.generateExcelLink = function()
	{
		var href = window.location.pathname + '?page=WC_Griffith_Subscriptions&format=excel';

		href += '&action=' + $scope.reportName;
		href += '&report=' + $scope.getSubscriptionResultType();
		href += '&status=' + $scope.getSubscriptionResultStatus();
		href += '&fields=' + $scope.fields.join();

		if( $scope.filters.numResults )
			href += '&limit=' + $scope.numResults;

		if( $scope.filters.dateStart )
			href += '&startdate=' + $scope.startDate;

		if( $scope.filters.dateEnd )
			href += '&enddate=' + $scope.endDate;

		return href;
	}

}]);


wcGriffithSubs.controller( 'ToolsCtrl', [ '$scope', '$http', '$filter', '$sanitize', function( $scope, $http, $filter )
{

	$scope.rolloverMsg = '';
	$scope.data = null;
	$scope.loading = false;
	$scope.rolloverEditionSelect = 0;
	$scope.rolloverStatus = 'publish';
	$scope.rolloverMaxSubs = 0;
	$scope.rollover = {

		articles: {
			execute: true,
			message: '',
			msgClass: 'error',
			requestTime: 0,
			order: 0
		},
		contributors: {
			execute: true,
			message: '',
			msgClass: 'error',
			requestTime: 0,
			order: 1
		},
		shopItem: {
			execute: true,
			message: '',
			msgClass: 'error',
			requestTime: 0,
			order: 2
		},
		expireSubs: {
			execute: true,
			message: '',
			msgClass: 'error',
			requestTime: 0,
			order: 3
		},
		edition: {
			execute: true,
			message: '',
			msgClass: 'error',
			requestTime: 0,
			order: 4
		}
	}

	$scope.executeRollover = function( $event )
	{
		$scope.rolloverMsg = '';

		if( typeof rolloverEditions == "undefined" ) {
			$scope.rolloverMsg = 'There are no editions to rollover.';
			return false;
		}

		$scope.data = null;

		var stack = $scope.getActionStack();
		var stackLength = stack.length;
		var currentAction = '';
		var previousAction = '';
		var totalSubs = 0;
		var subsProcessed = 0;
		var totalActionTime = 0;

		// Reset the message for any rollover actions being run
		angular.forEach(stack, function(value, key) {
			$scope.rollover[value].message = '';
		});

		var rolloverRequest = function( action ){
			$scope.loading = action;
			var startTime = new Date().getTime();
			$http({ method: 'POST',  url: ajaxurl, params: {
				action: 'gr_rollover_edition',
				rollover_action: action,
				rollover_status: $scope.rolloverStatus,
				max_num_subs: $scope.rolloverMaxSubs,
				edition_num: rolloverEditions[$scope.rolloverEditionSelect].editionNum, // rolloverEditions is defined in a script tag in tools.php
				edition_postid: rolloverEditions[$scope.rolloverEditionSelect].editionPostId
			}}).success(function( data, status, headers, config )
				{
					$scope.rollover[currentAction].requestTime = new Date().getTime() - startTime + $scope.rollover[currentAction].requestTime;
					$scope.loading = false;

					if( (typeof data === 'string' || data instanceof String) && data.indexOf('error') > -1 )
						$scope.rollover[currentAction].msgClass = 'error';
					else
						$scope.rollover[currentAction].msgClass = 'success';

					// if there is an error, set an error message and clear the stack
					if( $scope.rollover[currentAction].msgClass == 'error' ){
						$scope.rollover[currentAction].message = getErrorMessage( data );
						// Set previous action so the if check later can see it was an error
						previousAction = currentAction;
						currentAction = undefined;
					}
					// if expiring subs, check to see if everything has been expired or if we have to call again to expire more subs
					else if( currentAction == 'expireSubs' ) {

						// Set the total number of subscriptions on the response from the first expiration request
						if( totalSubs == 0 )
							totalSubs = data.total;

						// Add the number of subs processed so far, each time you process more subs
						subsProcessed += data.processed;

						$scope.rollover[currentAction].message = 'Expired '+subsProcessed+' of '+totalSubs+' ending subscriptions in '+($scope.rollover[currentAction].requestTime/1000)+' secs';;

						// Set previous action so we can check if it was successful before calling for more subs
						previousAction = currentAction;

						// If there are no subs left, move to the next action
						if( (totalSubs - subsProcessed) == 0 ) {
							currentAction = stack.shift();
						}
					}
					// all actions other than expiring subs
					else {
						$scope.rollover[currentAction].message = data+' in '+($scope.rollover[currentAction].requestTime/1000)+' secs';
						previousAction = currentAction;
						currentAction = stack.shift();
					}

					// if there is still an action to complete and the previous action was successful, continue to the next action
					if( typeof currentAction != 'undefined' && $scope.rollover[previousAction].msgClass == 'success' )
						rolloverRequest( currentAction );
				});
		}

		// Run the first action which will recursively call the remaining actions
		currentAction = stack.shift();
		if( typeof currentAction != 'undefined' )
			rolloverRequest( currentAction );
	}

	$scope.getActionStack = function()
	{
		var tempStack = new Array();
		var stack = new Array();
		var stackString = '';

		// Put selected actions into an initial stack
		for( action in $scope.rollover ) {
			if( true == $scope.rollover[action].execute )
				tempStack[$scope.rollover[action].order] = action;
		}

		var stackLength = tempStack.length;

		// Recreate stack without any missing elements
		for( i=0; i<stackLength; i++ ) {
			var action = tempStack.shift();
			if( typeof action != 'undefined' ) {
				stack.push(action);
			}
		}

		return stack;
	}

	$scope.getActionStackString = function()
	{
		var stackString = '';
		var stack = $scope.getActionStack();
		var stackLength = stack.length;

		// Recreate stack without any missing elements
		for( i=0; i<stackLength; i++ ) {
			var action = stack.shift();
			if( typeof action != 'undefined' ) {
				stackString += action+',';
			}
		}

		return stackString;
	}

}]);

wcGriffithSubs.controller( 'ImportStringsCtrl', [ '$scope', '$http', '$filter', '$sanitize', function( $scope, $http, $filter )
{
	$scope.msg = 'Loading packages and strings... ';
	$scope.loading = true;
  $scope.importStrings = {}

  // Get products
  $http({ method: 'GET',  url: ajaxurl, params: {
		ajax: true,
		action: 'get_subscription_products',
    categorise: false
	}}).success(function( data, status, headers, config )
		{
			$scope.subscriptionPackages = data;
			$scope.msg += ' Packages Loaded.';
		});

  // Get importStrings
	$http({ method: 'POST',  url: ajaxurl, params: {
		ajax: true,
		action: 'gr_get_setting',
		setting: 'import_strings'
	}}).success(function( data, status, headers, config )
		{
			$scope.importStrings = jQuery.extend( {}, $scope.importStrings, data );
			$scope.loading = false;
			$scope.msg += ' Strings Loaded.';
		});

	// Begin Functions
	$scope.setImportStrings = function(){
		$scope.msg = '';
		$scope.loading = true;

		$http({ method: 'POST',  url: ajaxurl, params: {
			ajax: true,
			action: 'gr_set_setting',
			setting: 'import_strings'
		}, data: $scope.importStrings }).success(function( data, status, headers, config )
		{
			//$scope.importStrings = data;
			$scope.loading = false;
			$scope.msg = 'All Strings Saved.'
		});
	}

}]);

var getErrorMessage = function( error ) {
	var message = error;

	if( error.indexOf( 'Maximum execution time' ) != -1 )
		message = 'It took too long for the server to perform this action and it timed out.';

	return message;
}

wcGriffithSubs.controller( 'DebugCtrl', [ '$scope', '$http', '$filter', '$sanitize', function( $scope, $http, $filter )
{
	$scope.msg = '';
	$scope.loading = false;
    $scope.inputOne = '';
    $scope.output = '';
    $scope.inputOneLabel = 'Select a debug function from above';
    $scope.inputLabels = {
        'getSubsMeantToBeActive': 'Enter the edition numbers separated by commas:',
				'getSubsMeantToExpire': 'Enter the edition numbers separated by commas:',
        'getSubsShouldNotHaveExpired': 'Enter the edition numbers separated by commas:'
    };

    $scope.runDebugFunction = function(event){
        $scope[$scope.debugFunction]();
    }

    $scope.updateLabels = function(event){
        $scope.inputOneLabel = $scope.inputLabels[$scope.debugFunction];
    }

    $scope.getSubsMeantToBeActive = function(){
        $scope.output = '';
        var editions = $scope.inputOne.split(",");
        var index = 0;

        // Reset the message for any rollover actions being run
        var runEdition = function( i ) {
            $scope.loading = true;
            $scope.msg = 'Loading...';

            $http({ method: 'POST',  url: ajaxurl, params: {
                action: 'ajax_debug',
                debug_function: 'output_subscriptions_that_should_be_subscribed_for_edition',
                edition_num: editions[i].trim()
            }}).success(function( data, status, headers, config ){
                $scope.loading = false;
                $scope.output += data;
                i++
                if( i < editions.length )
                    runEdition( i );
                else
                    $scope.msg = 'Debug Completed.';
            });
        }

        runEdition( index );
    }

		$scope.getSubsMeantToExpire = function(){
        $scope.output = '';
        var editions = $scope.inputOne.split(",");
        var index = 0;

        // Reset the message for any rollover actions being run
        var runEdition = function( i ) {
            $scope.loading = true;
            $scope.msg = 'Loading...';

            $http({ method: 'POST',  url: ajaxurl, params: {
                action: 'ajax_debug',
                debug_function: 'output_subscriptions_that_are_meant_to_expire_for_edition',
                edition_num: editions[i].trim()
            }}).success(function( data, status, headers, config ){
                $scope.loading = false;
                $scope.output += data;
                i++
                if( i < editions.length )
                    runEdition( i );
                else
                    $scope.msg = 'Debug Completed.';
            });
        }

        runEdition( index );
    }

    $scope.getSubsShouldNotHaveExpired = function(){
        $scope.output = '';
        var editions = $scope.inputOne.split(",");
        var index = 0;

        // Reset the message for any rollover actions being run
        var runEdition = function( i ) {
            $scope.loading = true;
            $scope.msg = 'Loading...';

            $http({ method: 'POST',  url: ajaxurl, params: {
                action: 'ajax_debug',
                debug_function: 'output_subscriptions_that_should_not_have_expired_for_edition',
                edition_num: editions[i].trim()
            }}).success(function( data, status, headers, config ){
                $scope.loading = false;
                $scope.output += data;
                i++
                if( i < editions.length )
                    runEdition( i );
                else
                    $scope.msg = 'Debug Completed.';
            });
        }

        runEdition( index );
    }

}]);

var getErrorMessage = function( error ) {
	var message = error;

	if( error.indexOf( 'Maximum execution time' ) != -1 )
		message = 'It took too long for the server to perform this action and it timed out.';

	return message;
}

Object.size = function(obj) {
  var size = 0, key;
  for (key in obj) {
    if (obj.hasOwnProperty(key)) size++;
  }
  return size;
};
