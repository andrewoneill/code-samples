<?php
/**
 * Subscriptions Importer Class
 *
 * A API of utility functions to import Griffith Review subscriptions from existing data sources.
 *  *
 * @package		WooCommerce Griffith Subscriptions/WC_Griffith_Subscriptions_Importer
 * @category	Class
 * @author		Andrew O'Neill <andrewdavidoneill@gmail.com>
 * @since			1.0
 */
require_once dirname(__FILE__) . '/lib/PHPExcel/PHPExcel.php';

class WC_Griffith_Subscriptions_Importer
{
	public static $address_field_map = array(
		"title" => "_user_title",
		"firstname" => "_first_name",
		"surname" => "_last_name",
		"company" => "_company",
		"positiontitle" => "_position_title",
		"address1" => "_address_1",
		"address2" => "_address_2",
		"suburbcity" => "_city",
		"state" => "_state",
		"zip" => "_postcode",
		"country" => "_country",
		"email" => "_email",
		"contact" => "_contact_frequency",
		"customerid" => "_old_customer_id",
		"phone" => "_phone"
	);

	public $standard_order_data_fields = array(
		'startedition',
		'packagetype',
		'gift',
		'username',
		'type'
	);

	public $subscription_products = array(
		"premium" => array(),
		"print" => array(),
		"digital" => array(),
		"double" => array(),
		"institutional" => array()
	);

	public $current_checkout = null;

	public $first_user_group = 'recipient';

	public function __construct()
	{
		// Ajax actions for reading a file's sheets
		add_action( 'wp_ajax_get_sheet_info', array( &$this, 'get_sheet_info' ) );
		add_action( 'wp_ajax_nopriv_get_sheet_info', array( &$this, 'get_sheet_info' ) );

		// Ajax actions for reading a file's sheets
		add_action( 'wp_ajax_get_sheet_data', array( &$this, 'get_sheet_data' ) );
		add_action( 'wp_ajax_nopriv_get_sheet_data', array( &$this, 'get_sheet_data' ) );

		// Ajax actions for creates orders for sheets
		add_action( 'wp_ajax_create_orders', array( &$this, 'create_orders' ) );
		add_action( 'wp_ajax_nopriv_create_orders', array( &$this, 'create_orders' ) );

		// Ajax actions for checking package types
		add_action( 'wp_ajax_check_packages', array( &$this, 'check_packages' ) );
		add_action( 'wp_ajax_nopriv_check_packages', array( &$this, 'check_packages' ) );

		// Ajax actions for checking package types
		add_action( 'wp_ajax_check_users', array( &$this, 'check_users' ) );
		add_action( 'wp_ajax_nopriv_check_users', array( &$this, 'check_users' ) );

		add_action( 'init', array( &$this, 'init' ) );
	}

	public function init()
	{
		if( is_admin() )
			{
				$this->init_subscription_products();
			}
	}

	public function init_subscription_products()
	{
		global $wpdb;

		$args = array(
			'post_type' => 'product',
			'posts_per_page' => -1,
			'tax_query' => array(
					array(
						'taxonomy' => 'product_cat',
						'field' => 'slug',
						'terms' => 'subscription-types'
					)
				)
		);
		$products = new WP_Query( $args );

		foreach( $products->posts as $product )
		{
			if( stripos( $product->post_name, 'digital' ) !== false )
				$this->subscription_products['digital'][$product->ID] = $product;
			else if( stripos( $product->post_title, 'print +' ) !== false )
				$this->subscription_products['premium'][$product->ID] = $product;
			else if( stripos( $product->post_name, 'print' ) !== false )
				$this->subscription_products['print'][$product->ID] = $product;
			else if( stripos( $product->post_name, 'institutional' ) !== false )
				$this->subscription_products['institutional'][$product->ID] = $product;
			else
				$this->subscription_products['double'][$product->ID] = $product;
		}

		/*foreach( $this->subscription_products as $type => $sub_children )
		{
			$products = $wpdb->get_results( "SELECT * FROM `wp_posts` WHERE `post_type` = 'product' AND `post_name` LIKE '%".$type."%subscription%'" );
			foreach( $products as $product ){
				if( stripos($product->post_name, $type) !== false )
				{
					$product = get_product( $product->ID );
					$children = $product->get_children();
					foreach( $children as $child )
					{
						$this->subscription_products[$type][$child] = get_product( $child );
					}
				}
			}
		}*/
        return $this->subscription_products;
	}

	public function get_from_payload( $key = 'sheets' )
	{
		$request_body = json_decode( file_get_contents('php://input') );

		switch( $key )
		{
			case 'sheets':
			case 'sheet_names':
				$sheet_names = array();
				foreach( $request_body->sheets as $sheet )
					$sheet_names[] = $sheet->worksheetName;
				return $sheet_names;
				break;

			case 'sheetFormat':
			case 'sheet_format':
				return $request_body->sheetFormat;
				break;
		}

	}

	public function setup_order_data_for_import( $sheet_data )
	{
		$order_fields = array();
		$package_types = array();

		foreach( $sheet_data as $name => $sheet )
		{
			$importNumber = 0;
			if( isset($request_body->importNumber) )
				$importNumber = (int)$request_body->importNumber;

			if( !$importNumber || $importNumber > $sheet->rows )
				$importNumber = $sheet->rows;

			for( $i=0; $i<$importNumber; $i++ )
			{
				$order_fields[$i] = array(
					"shiptobilling" => 0,
					"send_recipient_email" => 0,
					"subscription_gifter_id" => 0,
					"subscription_user_id" => 0,
					"gift_subscription" => 0,
					"start_edition" => 0,
					"product_id" => 0,
					"order_comments" => '',
					"account_username" => 0,
					"account_password" => 'letmein',
					"terms" => 0,
					"payment_method" => 'bacs',
					"imported_order" => 1,
					"import_user_only" => 0,
					"old_customer_id" => 0,
					"user_type" => 'individual'
				);

				// Make sure separate shipping and billing addresses are recorded
				$order_fields[$i]['ship_to_different_address'] = true;

				// If importing an order due for renewal, only create a user,  not an order
				if( stripos($name, 'Renewals') )
					$order_fields[$i]['import_user_only'] = 1;

				// Check for new package types and add to list if found
				if( !empty($sheet->order_data) ) {
					$package_types = array_merge( $package_types, $this->get_package_types($sheet->order_data) );

					// Copy across any non-standard orderdata fields for use when creating user/adding order data
					foreach( $sheet->order_data[$i] as $key => $value )
						if( !in_array( $key, $this->standard_order_data_fields ) )
							$order_fields[$i][$key] = $value;
				}

				// Add recipient data
				foreach( $sheet->recipient_data[$i] as $key => $value)
					if( array_key_exists($key, self::$address_field_map) )
						$order_fields[$i]['shipping'.self::$address_field_map[$key]] = $value;

				// Add gifter data
				foreach( $sheet->gifter_data[$i] as $key => $value)
					if( isset(self::$address_field_map[$key]) ) {
						if( ($key == 'firstname' || $key == 'surname') && strtoupper($value) == $value )
							$value = ucwords( strtolower($value) );
						$order_fields[$i]['billing'.self::$address_field_map[$key]] = $value;
					}

				// Set as institution if set so
				if( stripos($name, 'institutional') || ( array_key_exists( 'type', $sheet->order_data[$i] ) && stripos($sheet->order_data[$i]['type'], 'Institution') !== false ) || ( array_key_exists( 'packagetype', $sheet->order_data[$i] ) && stripos($sheet->order_data[$i]['packagetype'], 'Institution') !== false ) ) {
					$order_fields[$i]['user_type'] = 'institution';
					$order_fields[$i]['import_user_only'] = 1;

					if( isset( $order_fields[$i]['shipping_company'] ) )
						$order_fields[$i]['display_name'] = $order_fields[$i]['shipping_company'];
					if( isset( $order_fields[$i]['billing_company'] ) )
						$order_fields[$i]['display_name'] = $order_fields[$i]['billing_company'];
				}

				// Set old customer Id
				if( isset( $sheet->recipient_data[$i]['customerid'] ) )
					$order_fields[$i]['old_customer_id'] = $sheet->recipient_data[$i]['customerid'];

				// Check for gift subscription
				$order_fields[$i]['gift_subscription'] = (int)self::is_order_a_gift( $sheet->order_data[$i] );

				// Set username
				if( isset($sheet->order_data[$i]['username']) && trim($sheet->order_data[$i]['username']) != '' )
					$order_fields[$i]['account_username'] = $sheet->order_data[$i]['username'];

				else if( isset($order_fields[$i]['shipping_email']) && $order_fields[$i]['shipping_email'] != '' && $order_fields[$i]['gift_subscription'] !== 0 )
					$order_fields[$i]['account_username'] = $order_fields[$i]['shipping_email'];

				else if( isset($order_fields[$i]['billing_email']) && $order_fields[$i]['billing_email'] != '' && $order_fields[$i]['gift_subscription'] === 0 )
					$order_fields[$i]['account_username'] = $order_fields[$i]['billing_email'];

				// Add order data
				if( $order_fields[$i]['import_user_only'] === 0 ) {
					$order_fields[$i]['product_id'] = $package_types[$sheet->order_data[$i]['packagetype']];

					// Determine start edition from string
					if( is_numeric( $sheet->order_data[$i]['startedition'] ) )
						$order_fields[$i]['start_edition'] = (int)$sheet->order_data[$i]['startedition'];
					else {
						preg_match( '/\D*(\d+).*/', $sheet->order_data[$i]['startedition'], $matches );
						$order_fields[$i]['start_edition'] = (int)$matches[1];
					}

					$product = get_product( (int)$order_fields[$i]['product_id'] );
					$expiry_edition = (int)$order_fields[$i]['start_edition'] + (int)$product->subscription_length;
					$order_fields[$i]['expiry_edition'] = $expiry_edition;

					if( $expiry_edition <= WC_Griffith_Subscriptions::get_current_edition_num() )
						$order_fields[$i]['import_user_only'] = 1;
				}

			}
		}

		return $order_fields;
	}

	public function create_orders( $data )
	{
		global $woocommerce;
		global $current_user;
		global $WC_Griffith_Subscriptions;

		// Save extra subscription details into the order meta data on checkout
		add_action( 'woocommerce_checkout_update_order_meta', array( &$this, 'save_extra_details_on_checkout' ) );

		$available_gateways = $woocommerce->payment_gateways->get_available_payment_gateways();

		$sheet_names = $this->get_from_payload( 'sheet_names' );
		$sheet_data = self::get_sheet_data( $sheet_names, $this->get_from_payload( 'sheet_format' ) );

		/* Loop through the spreadsheet data and format the data for importing */
		$order_fields = $this->setup_order_data_for_import( $sheet_data );

		$order_awaiting_payment = $woocommerce->session->order_awaiting_payment;
		$woocommerce->session->order_awaiting_payment = 0;

		add_action( 'woocommerce_new_order', array( &$this, 'create_order' ) );

		$checkout_reflect = new ReflectionClass( 'WC_Checkout' );
		$checkout_user = $checkout_reflect->getProperty( "customer_id" );
		$checkout_user->setAccessible( true );
		$checkout_payment_method = $checkout_reflect->getProperty( "payment_method" );
		$checkout_payment_method->setAccessible( true );

		$created_orders = array();
		$users_without_orders = array();
		$skipped_rows = array();
		$ip_addresses = array();

		/*
			For each order added to $order_fields, create the order in woocommerce
		*/
		foreach( $order_fields as $num => $order )
		{
			$new_user_created = false;

			if( $this->first_user_group == 'recipient' && (int)$order['gift_subscription'] !== 0 )
				$account_fields = 'shipping';
			else
				$account_fields = 'billing';

			if( isset( $order['old_customer_id'] ) && $order['old_customer_id'] )
				$id = $order['old_customer_id'];
			else
				$id = $order[$account_fields.'_first_name'].' '.$order[$account_fields.'_last_name'];

			// If no username was set during the order setup, skip to the next order
			if( !$order['account_username'] ){
				$skipped_rows[$id] = "Customer with the ID of {$id} was not processed as they had no username or email specified.";
				continue;
			}

			/*if( !validate_username( $order['account_username'] ) ){
				$skipped_rows[$id] = "Customer with the ID of {$id} was not processed as their username is not in the correct format.";
				continue;
			}*/

			if( 0 === $order['import_user_only'] && isset($order['expiry_edition']) && $order['expiry_edition'] <= WC_Griffith_Subscriptions::get_current_edition_num() )
				$skipped_rows[$order['old_customer_id']] = "Customer subscription with the ID of {$order['old_customer_id']} began with Edition {$order['start_edition']} and has since expired.";

			$account = username_exists( $order['account_username'] );

			/*if( !$account )
				$account = email_exists( $order['shipping_email'] );

			if( !$account )
				$account = email_exists( $order['billing_email'] );*/

			if( !$account )
			{
				$new_user_created = true;

				$account = $WC_Griffith_Subscriptions->make_new_customer( $order, $account_fields );
	    		do_action( 'woocommerce_created_customer', $account );

	    		update_user_meta( $account, 'imported_user', 1 );
	    		update_user_meta( $account, 'user_type', $order['user_type'] );

				if( $order['user_type'] == 'institution' ) {
					if( isset($order['ipaddress']) )
						update_user_meta( $account, 'ip_address', $order['ipaddress'] );
					if( isset($order['notes']) )
						update_user_meta( $account, 'notes', trim($order['notes']) );

					update_user_meta( $account, 'subscribe_override', 1 );
				}

	    		if( isset($order[$account_fields.'_contact_frequency']) && $order[$account_fields.'_contact_frequency'] != '' )
	    		update_user_meta( $account, 'contact_frequency', $order[$account_fields.'_contact_frequency'] );
	    		else
	    		update_user_meta( $account, 'contact_frequency', 'any' );
			}
			$order_fields[$num]['subscription_gifter_id'] = $order['subscription_gifter_id'] = $account;
			$order_fields[$num]['subscription_user_id'] = $order['subscription_user_id'] = $account;
			$order_fields[$num]['customer_user'] = $order['customer_user'] = $account;

			// Import IP Addresses if they and the IP Login plugin exist
			if( function_exists('ipbl_admin_bar') && isset($order['ipaddress']) ) {
				$userdata = get_userdata( $account );

				// Split up multiple ip ranges
				$ip_ranges = explode( '|', $order['ipaddress'] );

				// Add each ip range
				foreach( $ip_ranges as $range ) {

					$range_split = explode( '-', $range );
					$start_ip = $range_split[0];
					// If there is a 2nd IP set that as the end of the range, otherwise use the start IP for the end
					$end_ip = isset($range_split[1]) ? $range_split[1] : $range_split[0];

					$ip_success = $this->add_ip_range( $userdata->data->user_login, $start_ip, $end_ip );

					if( !isset($ip_addresses[$userdata->data->user_login]) )
						$ip_addresses[$userdata->data->user_login] = $ip_success;
					else
						$ip_addresses[$userdata->data->user_login] .= ' | '.$ip_success;
				}

			}

			// If only importing the user, exit the loop here and continue to the next iteration
			if( 0 !== $order['import_user_only'] ) {
				if( $new_user_created ) {
					$user = get_userdata( $account );
					unset( $user->data->user_pass );
					unset( $user->data->user_activation_key );
					unset( $user->data->user_status );
					unset( $user->data->user_login );
					$users_without_orders[$account] = $user->data;
				}
				continue;
			}

			// Setup Cart
			$woocommerce->cart->empty_cart();
			$woocommerce->cart->base_recurring_prices = array( (int)$order['product_id'] => array() );
			$woocommerce->cart->recurring_cart_contents = array( $order['product_id'] => array() );
			$woocommerce->cart->add_to_cart( (int)$order['product_id'] );

			$_POST['subscription_user_id'] = $account;

			$this->current_checkout = new WC_Checkout();
			$this->current_checkout->checkout_fields['billing'] 	= $woocommerce->countries->get_address_fields( $order['billing_country'], 'billing_' );
			$this->current_checkout->checkout_fields['shipping'] 	= $woocommerce->countries->get_address_fields( $order['shipping_country'], 'shipping_' );
			$checkout_user->setValue( $this->current_checkout, $account );
			$checkout_payment_method->setValue( $this->current_checkout, $available_gateways[ $order['payment_method'] ] );
			$this->current_checkout->posted = $order;

			$order_id = $this->current_checkout->create_order();

			// Creating the order overwrites these fields if the billing user is different to shipping and the shipping info is supposed to be the default
			if( $account_fields == 'shipping' ) {
				WC_Griffith_Subscriptions::update_customer_details( $account, $order, $account_fields );
				wp_update_user( array(
					'ID' => $account,
					'user_email' => $order['shipping_email'],
					'first_name' => $order['shipping_first_name'],
      		'last_name' => $order['shipping_last_name']
				) );
			}

			$wc_order = new WC_Order( $order_id );
			WC_Subscriptions_Manager::update_users_subscriptions_for_order( $wc_order, 'pending' );
			$wc_order->update_status( 'completed' );
			do_action( 'subscriptions_created_for_order', $order_id );

			// Add extra fields if not an institution and they exist
			if( $order['user_type'] != 'institution' ) {
				if( isset($order['notes']) )
					update_post_meta( $order_id, 'notes', $order['notes'] );
			}

			if( isset($order['receiptnumber']) )
				update_post_meta( $order_id, 'receiptnumber', $order['receiptnumber'] );

			$wc_order = self::unset_excess_order_data( $wc_order );
			$wc_order->start_edition = $order['start_edition'];
			$wc_order->product_id = (int)$order['product_id'];
			$created_orders[$order_id] = $wc_order;

			unset( $_POST['subscription_user_id'] );
			$woocommerce->cart->empty_cart();
		}

		remove_action( 'woocommerce_new_order', array( &$this, 'create_order' ) );

		$woocommerce->session->order_awaiting_payment = $order_awaiting_payment;

		$products = new stdClass();
		foreach( $this->subscription_products as $group ){
			foreach( $group as $prod ) {
				$key = (int)$prod->ID;
				$products->$key = $prod;
			}
		}

		if( isset($_GET['action']) && $_GET['action'] == "create_orders" )
		{
			$return_data = array(
				'products' => $products,
				'orders' => $created_orders,
				'users_created' => $users_without_orders,
				'skipped_rows' => $skipped_rows,
				'ip_addresses' => $ip_addresses
			);

			echo json_encode( $return_data );

			//print_rf( $created_orders );
			//echo json_encode( $sheet_names );
			die();
		}

	}

	// Unused but hooked up
	public function create_order( $order_id )
	{

	}

	public function unset_excess_order_data( $order )
	{
		unset( $order->customer_note );
		unset( $order->order_custom_fields );
		unset( $order->payment_method );
		unset( $order->payment_method_title );
		unset( $order->order_discount );
		unset( $order->cart_discount );
		unset( $order->billing_address );
		unset( $order->formatted_billing_address );
		unset( $order->shipping_address );
		unset( $order->formatted_billing_address );
		unset( $order->prices_include_tax );
		unset( $order->tax_display_cart );
		unset( $order->display_totals_ex_tax );
		unset( $order->display_cart_ex_tax );
		unset( $order->recurring_payment_method );
		unset( $order->recurring_payment_method_title );
		return $order;
	}

	public function save_extra_details_on_checkout( $order_id )
	{
		// Add subscription_user_id to the post data so that the order customer_user can be modified
		$details = array( 'gift_subscription', 'subscription_user_id', 'subscription_gifter_id', 'send_recipient_email', 'customer_user', 'imported_order' );

		// Add meta data for each of the fields above if data exists in the POST variable
		foreach ( $this->current_checkout->posted as $field => $value ) {
			if ( $field == 'start_edition' )
				update_post_meta( $order_id, '_' . WC_Griffith_Subscriptions::$start_edition_meta_key_prefix . $this->current_checkout->posted['product_id'], esc_attr( $value ) );
			else if ( in_array( $field, $details ) )
				update_post_meta( $order_id, '_' . $field, esc_attr( $value ) );
		}
		return $order_id;
	}

	public function get_sheet_info()
	{
		$file = self::get_file_path_from_payload();
		$file_type = PHPExcel_IOFactory::identify( $file );
		$reader_obj = PHPExcel_IOFactory::createReader( $file_type );

		$sheet_names = $reader_obj->listWorksheetInfo( $file );

		if( isset($_GET['action']) && $_GET['action'] == "get_sheet_info" )
		{
			echo json_encode( $sheet_names );
			die();
		}
		return $sheet_names;
	}

	public function get_sheet_data( $sheet_names = array(), $sheet_format = 'oldsite', $iterate = true, $first_user_group = false )
	{
		$import_sheets = array();
		$request_body = json_decode( file_get_contents('php://input') );
		$file = self::get_file_path_from_payload();
		if( isset( $request_body->userOrder ) )
			$first_user_group = $request_body->firstUserGroup;
		else
			$first_user_group = $this->first_user_group;

		if( empty($sheet_names) )
			foreach( $request_body->sheets as $sheet )
				$sheet_names[] = $sheet->worksheetName;

		if( isset( $request_body->sheetFormat ) )
			$sheet_format = $request_body->sheetFormat;

		foreach( $sheet_names as $sheet )
			$import_sheets[$sheet] = new WC_Griffith_Import_Sheet( $file, $sheet, $sheet_format, $iterate, $first_user_group );

		if( isset($_GET['action']) && $_GET['action'] == "get_sheet_data" )
		{
			$output = array();
			foreach( $sheet_names as $sheet )
			{
				$output[$sheet] = array();
				$output[$sheet]['columns'] = $import_sheets[$sheet]->columns;
				$total_rows = count($import_sheets[$sheet]->recipient_data);
				for( $i = 0; $i < $total_rows; $i++ ) {
					$output[$sheet][$i] = array();
					$first_group = $import_sheets[$sheet]->first_user_group.'_data';
					$second_group = $import_sheets[$sheet]->get_alternate_user_group().'_data';

					$output[$sheet][$i][$first_group] = $import_sheets[$sheet]->{$first_group}[$i];
					$output[$sheet][$i][$second_group] = $import_sheets[$sheet]->{$second_group}[$i];
					$output[$sheet][$i]['order_data'] = $import_sheets[$sheet]->order_data[$i];
				}
				/*print_rf( $import_sheets[$sheet]->columns );
				print_rf( $import_sheets[$sheet]->recipient_data );
				print_rf( $import_sheets[$sheet]->gifter_data );
				print_rf( $import_sheets[$sheet]->order_data );*/
			}
			echo json_encode($output);
			die();
		}
		return $import_sheets;
	}

	public function check_packages()
	{
		$sheet_names = $this->get_from_payload( 'sheet_names' );
		$sheet_data = self::get_sheet_data( $sheet_names, $this->get_from_payload( 'sheet_format' ) );

		$package_types = array();
		foreach( $sheet_names as $sheet ) {
			//echo json_encode($sheet_data[$sheet]);
			if( !empty( $sheet_data[$sheet]->columns['order'] ) ) $data_array = "order_data";
			else if( !empty( $sheet_data[$sheet]->columns['gifter'] ) ) $data_array = "gifter_data";
			else if( !empty( $sheet_data[$sheet]->columns['recipient'] ) ) $data_array = "recipient_data";
			$package_types = array_merge( $package_types, self::get_package_types( $sheet_data[$sheet]->$data_array, true ) );
		}

		if( isset($_GET['action']) && $_GET['action'] == "check_packages" ) {
			echo json_encode( $package_types );
			die();
		}

		return $package_types;
	}

	public function check_users()
	{
		$sheet_names = $this->get_from_payload( 'sheet_names' );
		$sheet_data = self::get_sheet_data( $sheet_names, $this->get_from_payload( 'sheet_format' ) );

		$order_fields = $this->setup_order_data_for_import( $sheet_data );

		$users = array();

		foreach( $order_fields as $i => $order )
		{
			$users[$i] = array(
				'row' => $i+2,
				'username' => $order_fields[$i]['account_username']
			);

			if( isset($order_fields[$i]['shipping_email']) && $order_fields[$i]['shipping_email'] != '' && $order_fields[$i]['gift_subscription'] !== 0 )
			{
				$users[$i]['firstnNme'] = $order['shipping_first_name'];
				$users[$i]['lastName'] = $order['shipping_last_name'];
				$users[$i]['email'] = $order_fields[$i]['shipping_email'];
				$users[$i]['source'] = 'shipping';
				$users[$i]['gift'] = 'Yes';
			}
			else if( isset($order_fields[$i]['billing_email']) && $order_fields[$i]['billing_email'] != '' && $order_fields[$i]['gift_subscription'] === 0 )
			{
				$users[$i]['firstnNme'] = $order['billing_first_name'];
				$users[$i]['lastName'] = $order['billing_last_name'];
				$users[$i]['email'] = $order_fields[$i]['billing_email'];
				$users[$i]['source'] = 'billing';
				$users[$i]['gift'] = 'No';
			}

			$users[$i]['id'] = email_exists( $users[$i]['email'] );
			if(  false !== $users[$i]['id']  )
			{
				$users[$i]['message'] = "The email address is unavailable. Please use an alternate email address or delete the user currently using the intended email address.";
			}
			else
			{
				$users[$i]['message'] = "The email address is free to use.";
			}
		}

		echo json_encode( $users );
		die();
	}

	public function get_file_path_from_payload()
	{
		$request_body = json_decode( file_get_contents('php://input') );

		if( !empty($request_body) && is_int( $request_body->datafile ) )
			$input_filename = get_attached_file( $request_body->datafile );
		else if( !empty($request_body) )
			$input_filename = $request_body->datafile;

		return $input_filename;
	}

	public function get_package_types( $order_data, $keep_nomatch = false )
	{
		$types = array();
		foreach( $order_data as $order )
			if( array_key_exists('packagetype', $order) && !array_key_exists($order['packagetype'], $types) )
				$types[$order['packagetype']] = self::match_package_to_product( $order['packagetype'], 'Australia', $keep_nomatch );

		return $types;
	}

	public function match_package_to_product( $package, $country = 'Australia', $keep_nomatch = false )
	{
		$product_id = 0;
		$no_match = '';
		$type = $length = $format = null;
		$normal_types = array( 'premium', 'print', 'digital', 'institutional' );
		$preset_strings = WC_Griffith_Subscriptions::get_setting( 'import_strings' );
    
    if( $preset_strings ) {
      foreach($preset_strings as $id => $name) {
        if( $package == $name )
          return $id;
      }
    }		

		if( stripos( $package, 'granta' ) !== false ) $type = 'granta';
		else if( stripos( $package, 'island' ) !== false ) $type = 'island';
		else if( stripos( $package, 'meanjin' ) !== false ) $type = 'meanjin';
		else if( stripos( $package, 'overland' ) !== false ) $type = 'overland';
		else if( stripos( $package, 'southerly' ) !== false ) $type = 'southerly';
		else if( stripos( $package, 'institutional' ) !== false ) $type = 'institutional';
		else if( stripos( $package, 'institution' ) !== false ) $type = 'institutional';
		else if( stripos( $package, 'premium' ) !== false ) $type = 'premium';
		else if( stripos( $package, 'print' ) !== false ) $type = 'print';
		else if( stripos( $package, 'digital' ) !== false ) $type = 'digital';
		else if( stripos( $package, 'Three Year inc GST Australia only' ) !== false || stripos( $package, 'Three Years Outside Australia Only' ) !== false ) $type = 'print';
		else $no_match .= 'No type found in package name. ';

		preg_match( "{.*(\d)\s\S{4,5}.*}", $package, $matches );
		if( !empty( $matches ) ) {
			$length = $matches[1];
			if( (int)$length > 3 ) $length = 3; // Set longer subscriptions to 2 years
		}
		else if( is_null( $length ) ) {
			preg_match( "{.*(One|Two|Three)\s(Year).*}", $package, $matches );
			if( !empty( $matches ) ) {
				if( $matches[1] == "One" ) $length = 1;
				else if( $matches[1] == "Two" ) $length = 2;
				else if( $matches[1] == "Three" ) $length = 3;
				else $no_match .= "No numeric length found in the package name.\n";
			}
			else
				$no_match .= "No numeric length found in the package name.\n";
		}

		if( stripos($package, 'epub') !== false )
			$format = 'epub';
		else if( stripos($package, 'kindle') !== false )
			$format = 'kindle';
		else if( $type == 'institutional' ) {
			$no_match .= "Institutional subscription detected from package name. Users with this package will simply have a user created with subscription status enabled in their profile. No orders will appear in Woocommerce for users with this package.\n";
			if( stripos($package, '100 PLUS') !== false || stripos($package, '100+') !== false )
				$format = '100+';
			else if( stripos($package, 'up to 100') !== false )
				$format = 'up to 100';
			else if( stripos($package, 'Single') !== false )
				$format = 'Single';
		}
		else {
			$format = 'pdf';
			if( stripos($package, 'pdf') === false )
				$no_match .= "No format found in the package name, setting as pdf by default.\n";
		}

		if( $type == 'institutional' )
			$format = "AUST";
		else if( $type == 'print' && $country == 'Australia' && stripos($package, 'Australia') !== false ) {
			if( stripos($package, 'outside') !== false )
				$format = "International";
			else
				$format = "AUST";
		}
		else if( $type == 'print' )
			$format = 'International';
		else if( stripos($package, 'Australia') === false )
			$no_match .= "No country found in the package name, setting as Australia by default.\n";

		if( !empty( $type ) && !in_array( $type, $normal_types ) ) {
			foreach( $this->subscription_products['double'] as $id => $product ) {
				if( stripos($product->post_title, $type) !== false )
					return $id;
			}
		}
		else if( !empty( $type ) ) {
			foreach( $this->subscription_products[$type] as $id => $product ) {
				if( stripos($product->post_title, (string)$length) !== false && stripos($product->post_title, $format) !== false )
					return $id;
			}
		}

		if( $keep_nomatch && !empty( $no_match ) )
			return $no_match;

		/* ob_start();
		var_dump( $keep_nomatch );
		return ob_get_clean(); */
		return false;
	}

	public function is_order_a_gift( $order_data ) {
		$keys = array( 'gift', 'gift?', 'Gift', 'Gift?' );

		if( stripos( $order_data['packagetype'], 'gift' ) !== false )
			return true;

		$key = an_array_key_exists( $keys, $order_data );
		if( $key && stripos( $order_data[$key], 'yes' ) !== false )
			return true;

		return false;
	}

	// Requires IP Based Login plugin to work https://wordpress.org/plugins/ip-based-login/
	// Based on plugin version 1.3.7
	public function add_ip_range( $username, $start_ip, $end_ip ) {
		global $ip_based_login_options, $wpdb;

		$ip_based_login_options['username'] = esc_sql($username);
		$ip_based_login_options['start'] = esc_sql($start_ip);
		$ip_based_login_options['end'] = esc_sql($end_ip);

		/*print_r(esc_sql($username));
		echo "\n";
		print_r(esc_sql($start_ip));
		echo "\n";
		print_r(esc_sql($end_ip));
		echo "\n";
		print_r($ip_based_login_options);
		die();*/

		$user = get_user_by('login', $ip_based_login_options['username']);

		if(empty($user)){
			$error[] = 'The username does not exist.';
		}

		if(!valid_ip($ip_based_login_options['start'])){
			$error[] = 'Please provide a valid start IP';
		}

		if(!valid_ip($ip_based_login_options['end'])){
			$error[] = 'Please provide a valid end IP';
		}

		// This is to check if there is any other range exists with the same Start or End IP
		$ip_exists_query = "SELECT * FROM ".$wpdb->prefix."ip_based_login WHERE
		`start` BETWEEN '".ip2long($ip_based_login_options['start'])."' AND '".ip2long($ip_based_login_options['end'])."'
		OR `end` BETWEEN '".ip2long($ip_based_login_options['start'])."' AND '".ip2long($ip_based_login_options['end'])."';";
		$ip_exists = $wpdb->get_results($wpdb->prepare($ip_exists_query));
		//print_r($ip_exists);

		if(!empty($ip_exists)){
			$error[] = 'The Start IP or End IP submitted conflicts with an existing IP range!';
		}

		// This is to check if there is any other range exists with the same Start IP
		$start_ip_exists_query = "SELECT * FROM ".$wpdb->prefix."ip_based_login WHERE
		'".ip2long($ip_based_login_options['start'])."' BETWEEN `start` AND `end`;";
		$start_ip_exists = $wpdb->get_results($wpdb->prepare($start_ip_exists_query));
		//print_r($start_ip_exists);

		if(!empty($start_ip_exists)){
			$error[] = 'The Start IP is present in an existing range!';
		}

		// This is to check if there is any other range exists with the same End IP
		$end_ip_exists_query = "SELECT * FROM ".$wpdb->prefix."ip_based_login WHERE
		'".ip2long($ip_based_login_options['end'])."' BETWEEN `start` AND `end`;";
		$end_ip_exists = $wpdb->get_results($wpdb->prepare($end_ip_exists_query));
		//print_r($end_ip_exists);

		if(!empty($end_ip_exists)){
			$error[] = 'The End IP is present in an existing range!';
		}

		if(ip2long($ip_based_login_options['start']) > ip2long($ip_based_login_options['end'])){
			$error[] = 'The end IP cannot be smaller than the start IP';
		}

		if(empty($error)){

			$options['username'] = $ip_based_login_options['username'];
			$options['start'] = ip2long($ip_based_login_options['start']);
			$options['end'] = ip2long($ip_based_login_options['end']);
			$options['status'] = (is_checked('status') ? 1 : 0);
			$options['date'] = date('Ymd');

			$wpdb->insert($wpdb->prefix.'ip_based_login', $options);

			if(!empty($wpdb->insert_id)){
				return 'IP range added successfully';
			}else{
				return 'There were some errors while adding IP range';
			}

		}else{
			return $error;
		}
	}

}

class WC_Griffith_Import_Sheet
{
	public $columns = array(
		'recipient' => array(),
		'gifter' => array(),
		'order' => array()
	);

	public $recipient_data = array();

	public $gifter_data = array();

	public $order_data = array();

	public $field_order = array();

	public $rows = 0;

	public $first_user_group = 'recipient';

	public $file;

	public $sheet_formats = array(
		'oldsite' => array(
			"Customer ID",
			"Title",
			"First Name",
			"Surname",
			"Position Title",
			"Company",
			"Address1",
			"Address2",
			"Suburb/City",
			"State",
			"Zip",
			"Country",
			"Email",
			"Contact",
			"Title",
			"First Name",
			"Surname",
			"Position Title",
			"Company",
			"Address1",
			"Address2",
			"Suburb/City",
			"State",
			"Zip",
			"Country",
			"Email",
			"Contact",
			"Gift?",
			"Approval Date",
			"Package Type",
			"Start Edition",
			"Package",
			"Username",
			"IP addresses",
			"IP address",
			"Notes",
			"Receipt Number"
		)
	);

	public $PHPExcel_reader;

	public $PHPExcel_obj;

	public $address_fields = array(
		"firstname",
		"surname",
		"company",
		"address1",
		"address2",
		"suburbcity",
		"state",
		"zip",
		"country",
		"email",
		"contact",
		"customerid",
		"phone"
	);

	public static $contact_freq_map = array(
		"Any contact" => "any",
		"Any" => "any",
		"" => "any",
		"No contact" => "none",
		"Renewals only" => "renewals"
	);

	public function __construct( $file, $sheet_name = false, $sheet_format = 'oldsite', $iterate = true, $first_user_group = false )
	{
		$this->file = $file;
		$file_type = PHPExcel_IOFactory::identify( $this->file );
		$this->PHPExcel_reader = PHPExcel_IOFactory::createReader( $file_type );
		$this->PHPExcel_reader->setReadDataOnly( true );

		if( $first_user_group )
			$this->first_user_group = $first_user_group;

		if( $sheet_name )
			$this->PHPExcel_reader->setLoadSheetsOnly( array($sheet_name) );

		$this->PHPExcel_obj = $this->PHPExcel_reader->load( $this->file );

		if( class_exists( 'WC_Griffith_Subscriptions_Importer' ) )
			$this->address_fields = array_keys( WC_Griffith_Subscriptions_Importer::$address_field_map );

		$valid_format = $this->check_format( $sheet_name, $sheet_format );

		if( true === $valid_format && $iterate )
			self::iterate_sheet( $sheet_name );
		else {
			echo json_encode( array( 'missingFields' => $valid_format ) );
			die();
		}
	}

	public function check_format( $sheet_name, $sheet_format = 'oldsite' )
	{
		if( $sheet_name )
			$worksheet_obj = $this->PHPExcel_obj->getSheetByName( $sheet_name );
		else
			$worksheet_obj = $this->PHPExcel_obj->getActiveSheet();

		$format_fields = array();
		foreach( $this->sheet_formats[$sheet_format] as $field )	{
			$format_fields[] = sanitize_key( $field );
		}

		$fields = array();
		$broken_fields = array();

		foreach( $worksheet_obj->getRowIterator() as $row_index => $row )
		{
			$cell_iterator = $row->getCellIterator();
			$cell_iterator->setIterateOnlyExistingCells( FALSE );

			$key_offset = 0;
			foreach( $cell_iterator as $key => $cell )
			{
				$sanitized_key = sanitize_key( $cell->getValue() );

				if( $sanitized_key == '' )
					continue;

				if( isset($this->sheet_formats[$sheet_format][$key+$key_offset]) ) {

					if( in_array( $sanitized_key, $format_fields ) ) {
						$fields[] = $sanitized_key .' => '. $cell->getValue();
					}

					//if( strtolower($cell->getValue()) == strtolower($this->sheet_formats[$sheet_format][$key+$key_offset]) ) {
						//$fields[$this->sheet_formats[$sheet_format][$key+$key_offset]] = $cell->getValue();
					//}
					else {
						$broken_fields[] = 'Key: '.$key.' cell: '.$cell->getValue().' does not exist in specified sheet format.';
					}
				}
				else {
					$broken_fields[] = 'No corresponding key for: key: '.$key.' cell: '.$cell->getValue().' in specified sheet format.';
				}
			}
			break;
		}

		if( !empty($broken_fields) )
			return $broken_fields;
		else
			return true;
	}

	public function iterate_sheet( $sheet = false )
	{
		if( $sheet )
			$worksheet_obj = $this->PHPExcel_obj->getSheetByName( $sheet );
		else
			$worksheet_obj = $this->PHPExcel_obj->getActiveSheet();

		// Rows start as $row_index = 1
		foreach( $worksheet_obj->getRowIterator() as $row_index => $row )
		{
			$cell_iterator = $row->getCellIterator();
			$cell_iterator->setIterateOnlyExistingCells( FALSE );

			$recipient_data = array();
			$gifter_data = array();
			$order_data = array();

			$recipient_exists = false;
			$gifter_exists = false;

			// Set during cell iteration if the row is actually blank and only contains extra IP addresses
			$ip_only = true;

			// Cells start at $key = 0
			foreach( $cell_iterator as $key => $cell )
			{
				$obj = new stdClass();
				// If iterating the heading row
				if( $row_index === 1 )
					$this->get_columns( $cell );

				// If iterating content row
				else
				{
					$field = $this->field_order[$key]['field'];
					$group = $this->field_order[$key]['group'];

					if( $field == '' )
						continue;

					switch( $group ) {
						case 'gifter':
							if( $field == 'Contact' )
								$gifter_data[$field] = self::$contact_freq_map[trim($cell->getValue())];
							else
								$gifter_data[$field] = trim( $cell->getValue() );

							if( $field != 'Contact' && trim($gifter_data[$field]) != '' )
								$gifter_exists = true;
							break;

						case 'recipient':
							if( $field == 'Country' ) {
								$order_data[$field] = trim( $cell->getValue() );
								$recipient_data[$field] = trim( $cell->getValue() );
							}
							else if( $field == 'Contact' )
								$recipient_data[$field] = self::$contact_freq_map[trim( $cell->getValue() )];
							else
								$recipient_data[$field] = trim( $cell->getValue() );

							if( $field != 'Contact' && trim($recipient_data[$field]) != '' )
								$recipient_exists = true;
							break;

						case 'order':
						default:
							$order_data[$field] = trim( $cell->getValue() );
							if( $field != 'ipaddress' && $order_data[$field] != '' )
								$ip_only = false;

							// IP Address filtering
							else if( $field == 'ipaddress' ) {
								// do stuff
								$ip = false;

								if( !$ip ) {
									// check for ip range (0.0.0.0-1.1.1.1)
									preg_match("/((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)(\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|\*)){".'1,3'."})-((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)(\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|\*)){1,3})/", trim( $cell->getValue() ), $matches );
									if( !empty($matches) ) {
										$dot_count = substr_count( $matches[1], '.' );

										if( $dot_count == 3 ) {
											$ip = $matches[1] .'-'. $matches[5];
										}
										else if( $dot_count < 3 ) {
											$zeros = '';
											$end = '';
											for( $i = 0; $i < (3-$dot_count); $i++) {
												$zeros .= '.0';
												$end .= '.255';
											}
													$ip_split = explode( '.', $matches[1] );
													$ip_split[$dot_count] = str_replace( $matches[4], $matches[6], $ip_split[$dot_count] );
													$ip1 = $matches[1] . $zeros;
												$ip2 = implode( '.', $ip_split ) . $end;

											$ip = $ip1 . '-' . $ip2;
										}

										//ob_start();
										//print_r($matches);
										//$ip = ob_get_clean();
									}

									// if ip range not found, check for narrower range (0.0.0.0-1)
									if( !$ip ) {
											preg_match("/((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)(\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|\*)){3})-(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)/", trim( $cell->getValue() ), $matches);
										if( !empty($matches) ) {
											$first_ip = explode( '.', $matches[1] );
											$first_ip[3] = $matches[5];
											$second_ip = implode( '.', $first_ip );
											$ip = $matches[1] .'-'. $second_ip;
										}
									}
								}

								// if ip range was not found check for individual ips
								if( !$ip ) {
									preg_match("/((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)(\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|\*)){3})/", trim( $cell->getValue() ), $matches);

									if( !empty($matches) ) {
										$star_count = substr_count( $matches[1], '*' );
										if( $star_count ) {
											$ip = str_replace( '*', '0', $matches[1] );
											$ip .= '-'.str_replace( '*', '255', $matches[1] );
										}
										else
										 $ip = $matches[1];
									}
								}
								$order_data[$field] = $ip;
							} // END ip address filtering

							break;
					}
				}
			}

			if( $row_index > 1 )
			{
				$gifter_data["Phone"] = '';
				$recipient_data["Phone"] = '';

				if( ! $recipient_exists )
					$recipient_data = $gifter_data;

				if( ! $gifter_exists )
					$gifter_data = $recipient_data;

				// If the row only contains extra ip addresses, append to previous row. Otherwise add a new row.
				if( $ip_only ) {
					if( trim( $order_data['ipaddress'] ) != '' )
						$this->order_data[$this->rows-1]['ipaddress'] .= '|'.$order_data['ipaddress'];
					else
						continue;
				}
				else {
					$this->recipient_data[] = $recipient_data;
					$this->gifter_data[] = $gifter_data;
					$this->order_data[] = $order_data;
					$this->rows++;
				}
			}
		}
	}

	public function get_columns( $cell )
	{
		$col_name = sanitize_key( $cell->getValue() );

		if( in_array( $col_name, $this->address_fields ) )
		{
			if( $col_name == 'Customer ID' ) {
				$this->columns['recipient'][] = $col_name;
				$this->field_order[] = array(
					'field' => $col_name,
					'group' => 'recipient'
				);
			}
			else if( in_array( $col_name, $this->columns[$this->first_user_group] ) ) {
				$this->columns[$this->get_alternate_user_group()][] = $col_name;
				$this->field_order[] = array(
					'field' => $col_name,
					'group' => $this->get_alternate_user_group()
				);
			}
			else {
				$this->columns[$this->first_user_group][] = $col_name;
				$this->field_order[] = array(
					'field' => $col_name,
					'group' => $this->first_user_group
				);
			}
		}
		/*if( $key < 14 )
			$this->columns['gifter'][] = $col_name;
		else if( $key < 27 )
			$this->columns['recipient'][] = $col_name;*/
		else
		{
			if( stripos( $col_name, 'package' ) !== false ) {
				$this->columns['order'][] = 'packagetype';
				$this->field_order[] = array(
					'field' => 'packagetype',
					'group' => 'order'
				);
			}
			else if( stripos( $col_name, 'ipaddress' ) !== false ) {
				$this->columns['order'][] = 'ipaddress';
				$this->field_order[] = array(
					'field' => 'ipaddress',
					'group' => 'order'
				);
			}
			else {
				$this->columns['order'][] = $col_name;
				$this->field_order[] = array(
						'field' => $col_name,
						'group' => 'order'
					);
			}
		}
	}

	public function get_alternate_user_group()
	{
		if( $this->first_user_group === 'recipient' )
			return 'gifter';
		else
			return 'recipient';
	}
}

/**
 * Detect if the element is first in an array
 */
if( !function_exists( 'first' ) ) {
	function first($array, $key) {
	  reset($array);
	  return $key === key($array);
	}
}

/**
 * Detect if the element is last in an array
 */
if( !function_exists( 'last' ) ) {
	function last($array, $key) {
	  end($array);
	  return $key === key($array);
	}
}

/**
 * Loops through an array of keys to see if any of them exist in a given array
 */
function an_array_key_exists(array $keys, array $array)
{
  // Loop through all the keys and if one exist, return the key.
  foreach ( $keys as $key )
    if ( array_key_exists($key, $array) )
      return $key;

  // None of the keys were found.
  return false;
}


$WC_Griffith_Subscriptions_Importer = new WC_Griffith_Subscriptions_Importer();
?>
