<div class="wrap" ng-app="wcGriffithSubs">
	<h2><?php echo $admin_page_title ?></h2>
	<div ng-controller="ReportCtrl">
		<form method="post">
			<?php global $woocommerce; ?>

			<!-- Report Options -->
			<h3>Select report type:</h3>
			<table class="reports">
				<tr valign="top">
					<td style="width: 30px;">
						<input type="radio" ng-model="reportName" value="all_active_subscriptions" />
					</td>
					<td>All Active Subscriptions</td>
				</tr>
				<tr valign="top">
					<td style="width: 30px;">
						<input type="radio" ng-model="reportName" value="all_inactive_subscriptions" />
					</td>
					<td>All Inactive Subscriptions</td>
				</tr>
                <tr valign="top">
					<td style="width: 30px;">
						<input type="radio" ng-model="reportName" value="new" />
					</td>
					<td>New Subscriptions (within date range)</td>
				</tr>
				<tr valign="top">
					<td style="width: 30px;">
						<input type="radio" ng-model="reportName" value="lapsing" />
					</td>
					<td>Lapsing Subscriptions</td>
				</tr>
				<tr valign="top">
					<td style="width: 30px;">
						<input type="radio" ng-model="reportName" value="expiring" />
					</td>
					<td>Expiring Subscriptions</td>
				</tr>
				<tr valign="top">
					<td style="width: 30px;">
						<input type="radio" ng-model="reportName" value="daily_dispatch" />
					</td>
					<td>Daily Dispatch (print & premium subs, hard copy purchases from the online store with delivery details, product details, coupon code, coupon description)</td>
				</tr>
				<tr valign="top">
					<td style="width: 30px;">
						<input type="radio" ng-model="reportName" value="bulk_dispatch" />
					</td>
					<td>Bulk Dispatch</td>
				</tr>
			</table>

			<h3>Report filters:</h3>
			<table class="reports">
				<tr>
					<td>
						<input type="checkbox" ng-model="filters.numResults">
					</td>
					<td>
						<input type="number" ng-model="numResults" ng-disabled="!filters.numResults"/>
					</td>
					<td>Number of results <i>(set to zero for no limit)</i></td>
				</tr>
				<tr>
					<td>
						<input type="checkbox" ng-model="filters.dateStart">
					</td>
					<td>
						<input type="text" ng-model="startDate" id="start-date" ng-disabled="!filters.dateStart"/>
					</td>
					<td>Date range start</td>
				</tr>
				<tr>
					<td>
						<input type="checkbox" ng-model="filters.dateEnd">
					</td>
					<td>
						<input type="text" ng-model="endDate" id="end-date" ng-disabled="!filters.dateEnd"/>
					</td>
					<td>Date range end</td>
				</tr>
			</table>

			<!-- Actions -->
			<h3>Report actions:</h3>
			<table class="reports">
				<tr>
					<td>
						<a class="button-secondary" ng-click="logSubscriptionsData($event)" href="#">Get Subscriptions Data</a>
					</td>
					<td ng-if="loading"><img src="/wp-admin/images/wpspin_light.gif" alt="loading..." /></td>
					<td ng-if="data !== null" id="num-results"><strong>{{ getNumResults() }}</strong> results</td>
				</tr>
			</table>

			<div id="results" ng-if="data">
				<hr>
				<style id="results-table-style">
					.results-table{
						font-size: 12px;
						font-family: Arimo, 'Liberation Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif;
					}
					.results-table tr:nth-child(odd){
						background: #efefef;
					}
				</style>
				<a class="button-secondary" ng-click="openResultsWindow($event)" href="#">View results in a larger window</a>
				<!-- <a class="button-primary" ng-click="export($event)" ng-href="{{ window.location.pathname }}?page=WC_Griffith_Subscriptions&format=csv&action={{ reportName }}&report={{ getSubscriptionResultType() }}&limit={{ numResults }}&status={{ getSubscriptionResultStatus() }}&startdate={{ startDate }}&enddate={{ endDate }}">Export CSV</a> -->
				<a class="button-primary" ng-click="export($event)" ng-href="{{generateExcelLink()}}">Export Excel</a>
				<span style="vertical-align: middle;" ng-if="exporting">The file is being generated and will download shortly. Depending on the date range specified, this process may take several minutes.</span>
				<br />

				<h3 ng-if="data.subscriptions">Subscriptions</h3>
				<table id="results-table" class="results-table widefat" ng-if="data.subscriptions">
					<tr>
						<th class="row-title" colspan="13">Recipient Info</th>
						<th class="row-title" ng-if="jQuery.inArray('gifter', fields) !== -1" colspan="14">Gifter Info</th>
						<th class="row-title" colspan="7">Order Info</th>
					</tr>
					<tr ng-if="!data.subscriptions.columns">
						<th class="row-title" >User ID</th>
						<th class="row-title" >Username</th>
						<th class="row-title" >First Name</th>
						<th class="row-title" >Surname</th>
						<th class="row-title" >Company</th>
						<th class="row-title" >Address 1</th>
						<th class="row-title" >Address 2</th>
						<th class="row-title" >Suburb/City</th>
						<th class="row-title" >State</th>
						<th class="row-title" >Postcode</th>
						<th class="row-title" >Country</th>
						<th class="row-title" >Email</th>
						<th class="row-title" >Contact Frequency</th>

						<th class="row-title" ng-if="jQuery.inArray('gifter', fields) !== -1">User ID</th>
						<th class="row-title" ng-if="jQuery.inArray('gifter', fields) !== -1">Username</th>
						<th class="row-title" ng-if="jQuery.inArray('gifter', fields) !== -1">First Name</th>
						<th class="row-title" ng-if="jQuery.inArray('gifter', fields) !== -1">Surname</th>
						<th class="row-title" ng-if="jQuery.inArray('gifter', fields) !== -1">Company</th>
						<th class="row-title" ng-if="jQuery.inArray('gifter', fields) !== -1">Address 1</th>
						<th class="row-title" ng-if="jQuery.inArray('gifter', fields) !== -1">Address 2</th>
						<th class="row-title" ng-if="jQuery.inArray('gifter', fields) !== -1">Suburb/City</th>
						<th class="row-title" ng-if="jQuery.inArray('gifter', fields) !== -1">State</th>
						<th class="row-title" ng-if="jQuery.inArray('gifter', fields) !== -1">Postcode</th>
						<th class="row-title" ng-if="jQuery.inArray('gifter', fields) !== -1">Country</th>
						<th class="row-title" ng-if="jQuery.inArray('gifter', fields) !== -1">Email</th>
						<th class="row-title" ng-if="jQuery.inArray('gifter', fields) !== -1">Phone</th>
						<th class="row-title" ng-if="jQuery.inArray('gifter', fields) !== -1">Contact Frequency</th>

						<th class="row-title" >Order ID</th>
						<th class="row-title" >Order Date</th>
						<th class="row-title" >Order Status</th>
						<th class="row-title" >Package Type</th>
						<th class="row-title" >Start Edition</th>
						<th class="row-title" >Package Name</th>

						<th class="row-title" >Coupons</th>
						<th class="row-title" >Renewal</th>
					</tr>
					<tr ng-repeat="(name, result) in data.subscriptions">
						<td>{{ result.recipient_user_id }}</td>
						<td>{{ result.recipient_username }}</td>
						<td>{{ result.recipient_first_name }}</td>
						<td>{{ result.recipient_surname }}</td>
						<td>{{ result.recipient_company }}</td>
						<td>{{ result.recipient_address1 }}</td>
						<td>{{ result.recipient_address2 }}</td>
						<td>{{ result.recipient_suburb_city }}</td>
						<td>{{ result.recipient_state }}</td>
						<td>{{ result.recipient_postcode }}</td>
						<td>{{ result.recipient_country }}</td>
						<td>{{ result.recipient_email }}</td>
						<td>{{ result.recipient_contact_frequency }}</td>

						<td ng-if="jQuery.inArray('gifter', fields) !== -1">{{ result.gifter_user_id }}</td>
						<td ng-if="jQuery.inArray('gifter', fields) !== -1">{{ result.gifter_username }}</td>
						<td ng-if="jQuery.inArray('gifter', fields) !== -1">{{ result.gifter_first_name }}</td>
						<td ng-if="jQuery.inArray('gifter', fields) !== -1">{{ result.gifter_surname }}</td>
						<td ng-if="jQuery.inArray('gifter', fields) !== -1">{{ result.gifter_company }}</td>
						<td ng-if="jQuery.inArray('gifter', fields) !== -1">{{ result.gifter_address1 }}</td>
						<td ng-if="jQuery.inArray('gifter', fields) !== -1">{{ result.gifter_address2 }}</td>
						<td ng-if="jQuery.inArray('gifter', fields) !== -1">{{ result.gifter_suburb_city }}</td>
						<td ng-if="jQuery.inArray('gifter', fields) !== -1">{{ result.gifter_state }}</td>
						<td ng-if="jQuery.inArray('gifter', fields) !== -1">{{ result.gifter_postcode }}</td>
						<td ng-if="jQuery.inArray('gifter', fields) !== -1">{{ result.gifter_country }}</td>
						<td ng-if="jQuery.inArray('gifter', fields) !== -1">{{ result.gifter_email }}</td>
						<td ng-if="jQuery.inArray('gifter', fields) !== -1">{{ result.gifter_phone }}</td>
						<td ng-if="jQuery.inArray('gifter', fields) !== -1">{{ result.gifter_contact_frequency }}</td>

						<td>{{ result.order_id }}</td>
						<td>{{ result.order_date }}</td>
						<td>{{ result.order_status }}</td>
						<td>{{ result.package_type }}</td>
						<td>{{ result.start_edition }}</td>
						<td>{{ result.package_name }}</td>

						<td>{{ result.coupons }}</td>
						<td>{{ result.renewal }}</td>
					</tr>
				</table>

				<h3 ng-if="data.editions">Editions</h3>
				<table class="results-table widefat" ng-if="data.editions">
					<tr>
						<th class="row-title" colspan="13">Shipping Info</th>
						<th class="row-title" colspan="11">Billing Info</th>
						<th class="row-title" colspan="6">Order Info</th>
					</tr>
					<tr ng-if="!data.editions.columns">
						<th class="row-title" >User ID</th>
						<th class="row-title" >Username</th>
						<th class="row-title" >First Name</th>
						<th class="row-title" >Surname</th>
						<th class="row-title" >Company</th>
						<th class="row-title" >Address 1</th>
						<th class="row-title" >Address 2</th>
						<th class="row-title" >Suburb/City</th>
						<th class="row-title" >State</th>
						<th class="row-title" >Postcode</th>
						<th class="row-title" >Country</th>
						<th class="row-title" >Email</th>
						<th class="row-title" >Contact Frequency</th>

						<th class="row-title" >First Name</th>
						<th class="row-title" >Surname</th>
						<th class="row-title" >Company</th>
						<th class="row-title" >Address 1</th>
						<th class="row-title" >Address 2</th>
						<th class="row-title" >Suburb/City</th>
						<th class="row-title" >State</th>
						<th class="row-title" >Postcode</th>
						<th class="row-title" >Country</th>
						<th class="row-title" >Email</th>
						<th class="row-title" >Phone</th>

						<th class="row-title" >Order ID</th>
						<th class="row-title" >Order Date</th>
						<th class="row-title" >Order Status</th>
						<th class="row-title" >Format</th>
						<th class="row-title" >Package Name</th>
						<th class="row-title" >Edition No.</th>

						<th class="row-title" >Coupons</th>
					</tr>
					<tr ng-repeat="(name, result) in data.editions">
						<td>{{ result.customer_user_id }}</td>
						<td>{{ result.customer_username }}</td>
						<td>{{ result.shipping_first_name }}</td>
						<td>{{ result.shipping_surname }}</td>
						<td>{{ result.shipping_company }}</td>
						<td>{{ result.shipping_address1 }}</td>
						<td>{{ result.shipping_address2 }}</td>
						<td>{{ result.shipping_suburb_city }}</td>
						<td>{{ result.shipping_state }}</td>
						<td>{{ result.shipping_postcode }}</td>
						<td>{{ result.shipping_country }}</td>
						<td>{{ result.shipping_email }}</td>
						<td>{{ result.contact_frequency }}</td>

						<td>{{ result.billing_first_name }}</td>
						<td>{{ result.billing_surname }}</td>
						<td>{{ result.billing_company }}</td>
						<td>{{ result.billing_address1 }}</td>
						<td>{{ result.billing_address2 }}</td>
						<td>{{ result.billing_suburb_city }}</td>
						<td>{{ result.billing_state }}</td>
						<td>{{ result.billing_postcode }}</td>
						<td>{{ result.billing_country }}</td>
						<td>{{ result.billing_email }}</td>
						<td>{{ result.billing_phone }}</td>

						<td>{{ result.order_id }}</td>
						<td>{{ result.order_date }}</td>
						<td>{{ result.order_status }}</td>
						<td>{{ result.format }}</td>
						<td>{{ result.package_name }}</td>
						<td>{{ result.edition_num }}</td>

						<td>{{ result.coupons }}</td>
					</tr>
				</table>

				<h3 ng-if="data.digital_singles">Digital eSingles</h3>
				<table class="results-table widefat" ng-if="data.digital_singles">
					<thead>
						<tr>
							<th class="row-title" colspan="13">Shipping Info</th>
							<th class="row-title" colspan="11">Billing Info</th>
							<th class="row-title" colspan="6">Order Info</th>
						</tr>

						<tr ng-if="!data.digital_singles.columns">
							<th class="row-title" >User ID</th>
							<th class="row-title" >Username</th>
							<th class="row-title" >First Name</th>
							<th class="row-title" >Surname</th>
							<th class="row-title" >Company</th>
							<th class="row-title" >Address 1</th>
							<th class="row-title" >Address 2</th>
							<th class="row-title" >Suburb/City</th>
							<th class="row-title" >State</th>
							<th class="row-title" >Postcode</th>
							<th class="row-title" >Country</th>
							<th class="row-title" >Email</th>
							<th class="row-title" >Contact Frequency</th>

							<th class="row-title" >First Name</th>
							<th class="row-title" >Surname</th>
							<th class="row-title" >Company</th>
							<th class="row-title" >Address 1</th>
							<th class="row-title" >Address 2</th>
							<th class="row-title" >Suburb/City</th>
							<th class="row-title" >State</th>
							<th class="row-title" >Postcode</th>
							<th class="row-title" >Country</th>
							<th class="row-title" >Email</th>
							<th class="row-title" >Phone</th>

							<th class="row-title" >Order ID</th>
							<th class="row-title" >Order Date</th>
							<th class="row-title" >Order Status</th>
							<th class="row-title" >Package Name</th>
							<th class="row-title" >Edition No.</th>

							<th class="row-title" >Coupons</th>
						</tr>
					</thead>
					<tbody>
						<tr ng-repeat="(name, result) in data.digital_singles">
							<td>{{ result.customer_user_id }}</td>
							<td>{{ result.customer_username }}</td>
							<td>{{ result.shipping_first_name }}</td>
							<td>{{ result.shipping_surname }}</td>
							<td>{{ result.shipping_company }}</td>
							<td>{{ result.shipping_address1 }}</td>
							<td>{{ result.shipping_address2 }}</td>
							<td>{{ result.shipping_suburb_city }}</td>
							<td>{{ result.shipping_state }}</td>
							<td>{{ result.shipping_postcode }}</td>
							<td>{{ result.shipping_country }}</td>
							<td>{{ result.shipping_email }}</td>
							<td>{{ result.contact_frequency }}</td>

							<td>{{ result.billing_first_name }}</td>
							<td>{{ result.billing_surname }}</td>
							<td>{{ result.billing_company }}</td>
							<td>{{ result.billing_address1 }}</td>
							<td>{{ result.billing_address2 }}</td>
							<td>{{ result.billing_suburb_city }}</td>
							<td>{{ result.billing_state }}</td>
							<td>{{ result.billing_postcode }}</td>
							<td>{{ result.billing_country }}</td>
							<td>{{ result.billing_email }}</td>
							<td>{{ result.billing_phone }}</td>

							<td>{{ result.ID }}</td>
							<td>{{ result.order_date }}</td>
							<td>{{ result.order_status }}</td>
							<td>
								<div class="order-item" ng-repeat="item in result.order_items">
									{{ item.package_name }} - {{ item.format }}
								</div>
							</td>

							<td>{{ result.coupons }}</td>
						</tr>
					</tbody>
				</table>

				<div class="product-category" ng-repeat="(title, category) in data">
					<h3 ng-if="(title != 'subscriptions' && title != 'editions' && title != 'digital_singles')">{{ title }}</h3>
					<table class="results-table widefat" ng-if="(title != 'subscriptions' && title != 'editions' && title != 'digital_singles')">
						<thead>
							<tr>
								<th class="row-title" colspan="13">Shipping Info</th>
								<th class="row-title" colspan="11">Billing Info</th>
								<th class="row-title" colspan="6">Order Info</th>
							</tr>

							<tr ng-if="!category.columns">
								<th class="row-title" >User ID</th>
								<th class="row-title" >Username</th>
								<th class="row-title" >First Name</th>
								<th class="row-title" >Surname</th>
								<th class="row-title" >Company</th>
								<th class="row-title" >Address 1</th>
								<th class="row-title" >Address 2</th>
								<th class="row-title" >Suburb/City</th>
								<th class="row-title" >State</th>
								<th class="row-title" >Postcode</th>
								<th class="row-title" >Country</th>
								<th class="row-title" >Email</th>
								<th class="row-title" >Contact Frequency</th>

								<th class="row-title" >First Name</th>
								<th class="row-title" >Surname</th>
								<th class="row-title" >Company</th>
								<th class="row-title" >Address 1</th>
								<th class="row-title" >Address 2</th>
								<th class="row-title" >Suburb/City</th>
								<th class="row-title" >State</th>
								<th class="row-title" >Postcode</th>
								<th class="row-title" >Country</th>
								<th class="row-title" >Email</th>
								<th class="row-title" >Phone</th>

								<th class="row-title" >Order ID</th>
								<th class="row-title" >Order Date</th>
								<th class="row-title" >Order Status</th>
								<th class="row-title" >Package Name</th>
								<th class="row-title" >Edition No.</th>

								<th class="row-title" >Coupons</th>
							</tr>
						</thead>
						<tbody>
							<tr ng-repeat="(name, result) in category">
								<td>{{ result.customer_user_id }}</td>
								<td>{{ result.customer_username }}</td>
								<td>{{ result.shipping_first_name }}</td>
								<td>{{ result.shipping_surname }}</td>
								<td>{{ result.shipping_company }}</td>
								<td>{{ result.shipping_address1 }}</td>
								<td>{{ result.shipping_address2 }}</td>
								<td>{{ result.shipping_suburb_city }}</td>
								<td>{{ result.shipping_state }}</td>
								<td>{{ result.shipping_postcode }}</td>
								<td>{{ result.shipping_country }}</td>
								<td>{{ result.shipping_email }}</td>
								<td>{{ result.contact_frequency }}</td>

								<td>{{ result.billing_first_name }}</td>
								<td>{{ result.billing_surname }}</td>
								<td>{{ result.billing_company }}</td>
								<td>{{ result.billing_address1 }}</td>
								<td>{{ result.billing_address2 }}</td>
								<td>{{ result.billing_suburb_city }}</td>
								<td>{{ result.billing_state }}</td>
								<td>{{ result.billing_postcode }}</td>
								<td>{{ result.billing_country }}</td>
								<td>{{ result.billing_email }}</td>
								<td>{{ result.billing_phone }}</td>

								<td>{{ result.ID }}</td>
								<td>{{ result.order_date }}</td>
								<td>{{ result.order_status }}</td>
								<td>
									<div class="order-item" ng-repeat="item in result.order_items">
										{{ item.package_name }} - {{ item.format }}
									</div>
								</td>

								<td>{{ result.coupons }}</td>
							</tr>
						</tbody>
					</table>
				</div>

			</div>

		</form>
	</div>
</div>
